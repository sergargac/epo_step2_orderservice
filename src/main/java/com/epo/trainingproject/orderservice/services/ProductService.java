package com.epo.trainingproject.orderservice.services;

import com.epo.trainingproject.orderservice.domain.Stock;
import com.epo.trainingproject.orderservice.model.ProductModel;

public interface ProductService {

    ProductModel save(ProductModel productModel);

    Stock updateStock(Integer id, Integer amount);

    boolean checkStock(Integer id);

}
