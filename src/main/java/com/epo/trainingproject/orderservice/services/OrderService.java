package com.epo.trainingproject.orderservice.services;

import com.epo.trainingproject.orderservice.model.ProductWrapper;
import org.springframework.http.ResponseEntity;

public interface OrderService {

    public ResponseEntity makeOrder(ProductWrapper products);

}
