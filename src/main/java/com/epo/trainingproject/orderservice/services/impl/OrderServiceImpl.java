package com.epo.trainingproject.orderservice.services.impl;

import com.epo.trainingproject.orderservice.domain.Orders;
import com.epo.trainingproject.orderservice.domain.Product;
import com.epo.trainingproject.orderservice.model.ProductWrapper;
import com.epo.trainingproject.orderservice.domain.Stock;
import com.epo.trainingproject.orderservice.repository.OrderRepository;
import com.epo.trainingproject.orderservice.repository.ProductRepository;
import com.epo.trainingproject.orderservice.repository.StockRepository;
import com.epo.trainingproject.orderservice.services.OrderService;
import com.epo.trainingproject.orderservice.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private static final String BASE_URL = "http://localhost:8091/";
    private static final String URI = "shipping/ship";

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private ProductService productService;

    Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Override
    public ResponseEntity makeOrder(ProductWrapper products) {
        float price = 0;
        List<Product> productsOrder = new ArrayList<>();
        List<Integer> productIds = new ArrayList<>();

        for(Product product : products.getProducts()){
            Optional<Product> prd = productRepository.findById(product.getId());
            if(prd.isPresent()){
                productsOrder.add(prd.get());
                productIds.add(prd.get().getId());
                price = price + prd.get().getPrice();
                Optional<Stock> stock = stockRepository.findByProductId(prd.get().getId());
                if(stock.isPresent() || stock.get().getAmount() <= 0){
                    logger.info("Stock not available for product: "+prd.get().getId()+", increasing stock by 5 units.");
                    productService.updateStock(prd.get().getId(), 5);
                }
            }
        }

        Orders orders = new Orders();
        orders.setProducts(productsOrder);
        orders.setTotalPrice(price);
        orderRepository.save(orders);

        WebClient webClient = WebClient.builder()
                .baseUrl(BASE_URL)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();

        Integer response = webClient.post()
                .uri(URI)
                .body(BodyInserters.fromPublisher(Mono.just(productIds), List.class))
                .retrieve()
                .bodyToMono(Integer.class)
                .block();

        if(response == 200){
            logger.info("Order shipped correctly.");
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            logger.info("Error when shipping order.");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }
}
