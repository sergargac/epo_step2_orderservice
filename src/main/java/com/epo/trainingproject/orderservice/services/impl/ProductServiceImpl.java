package com.epo.trainingproject.orderservice.services.impl;

import com.epo.trainingproject.orderservice.domain.Product;
import com.epo.trainingproject.orderservice.domain.Stock;
import com.epo.trainingproject.orderservice.model.ProductModel;
import com.epo.trainingproject.orderservice.repository.ProductRepository;
import com.epo.trainingproject.orderservice.repository.ProductTypesRepository;
import com.epo.trainingproject.orderservice.repository.StockRepository;
import com.epo.trainingproject.orderservice.services.ProductService;
import com.epo.trainingproject.orderservice.util.ProductConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private ProductConverter productConverter;

    @Autowired
    private ProductTypesRepository productTypesRepository;

    @Override
    public ProductModel save(ProductModel productModel) {

        Product product = productConverter.modelToEntity(productModel);

        productTypesRepository.findByType(productModel.getProductType()).ifPresent(product::setProductTypes);

        Stock newStock = Stock.builder()
                .product(product)
                .build();
        product.setStock(newStock);

        return productConverter.entityToModel(productRepository.save(product));
    }

    @Override
    public boolean checkStock(Integer id) {
        Optional<Stock> stock = stockRepository.findByProductId(id);
        if(stock.isPresent() && stock.get().getAmount()>0){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public Stock updateStock(Integer id, Integer amount) {

        if (amount <= 0) {
            throw new RuntimeException("Quantity should be a positive value above zero.");
        }

        Optional<Stock> stock = stockRepository.findById(id);
        if (stock.isPresent()) {
            Stock updatedStock = stock.get();
            updatedStock.setAmount(amount);
            return stockRepository.save(updatedStock);
        } else {
            throw new NoSuchElementException("Product with id " + id + " does not exist in stock.");
        }
    }
}
