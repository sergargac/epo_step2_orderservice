package com.epo.trainingproject.orderservice.util;

import com.epo.trainingproject.orderservice.domain.Product;
import com.epo.trainingproject.orderservice.domain.ProductTypes;
import com.epo.trainingproject.orderservice.model.ProductModel;
import org.springframework.stereotype.Component;

@Component
public class ProductConverter {

    public Product modelToEntity(ProductModel productModel){

        ProductTypes productTypes = ProductTypes.builder()
                .type(productModel.getProductType())
                .build();

        return Product.builder()
                .id(productModel.getId())
                .name(productModel.getName())
                .description(productModel.getDescription())
                .price(productModel.getPrice())
                .productTypes(productTypes)
                .build();
    }

    public ProductModel entityToModel(Product product){
        return ProductModel.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .productType(product.getProductTypes().getType())
                .build();
    }

}
