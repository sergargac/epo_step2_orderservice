package com.epo.trainingproject.orderservice.repository;

import com.epo.trainingproject.orderservice.domain.ProductTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductTypesRepository extends JpaRepository<ProductTypes, Integer> {
    Optional<ProductTypes> findByType(String type);
}
