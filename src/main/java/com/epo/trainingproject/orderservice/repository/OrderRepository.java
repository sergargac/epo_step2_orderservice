package com.epo.trainingproject.orderservice.repository;

import com.epo.trainingproject.orderservice.domain.Orders;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<Orders, Integer> {
}
