package com.epo.trainingproject.orderservice.repository;

import com.epo.trainingproject.orderservice.domain.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StockRepository extends JpaRepository<Stock, Integer> {
    Optional<Stock> findByProductId(Integer id);
}
