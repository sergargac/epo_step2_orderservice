package com.epo.trainingproject.orderservice.model;

import com.epo.trainingproject.orderservice.domain.Product;
import lombok.Data;

import java.util.List;

@Data
public class ProductWrapper {

    private List<Product> products;

}
