package com.epo.trainingproject.orderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class OrderserviceApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(OrderserviceApplication.class, args);
	}

}
    