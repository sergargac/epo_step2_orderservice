package com.epo.trainingproject.orderservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ORDERS")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private Integer id;

    @ManyToMany
    @JoinTable(
            name = "PRODUCT_ORDER",
            joinColumns = @JoinColumn(name = "FK_ORDERS_ID", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "FK_PRODUCT_ID", nullable = false)
    )
    private List<Product> products = new ArrayList<>();

    private Float totalPrice;
}
