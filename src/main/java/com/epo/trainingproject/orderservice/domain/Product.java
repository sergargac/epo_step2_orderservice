package com.epo.trainingproject.orderservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="PRODUCT_TYPES_ID")
    private ProductTypes productTypes;

    @OneToOne(cascade = {CascadeType.ALL}, mappedBy = "product")
    private Stock stock;

    private String name;
    private String description;
    private Float price;

}
