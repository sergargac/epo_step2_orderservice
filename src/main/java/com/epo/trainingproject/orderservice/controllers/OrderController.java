package com.epo.trainingproject.orderservice.controllers;

import com.epo.trainingproject.orderservice.model.ProductWrapper;
import com.epo.trainingproject.orderservice.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping("order/create")
    public ResponseEntity orderCreate(@RequestBody ProductWrapper products){
        return orderService.makeOrder(products);
    }

}
