package com.epo.trainingproject.orderservice.controllers;

import com.epo.trainingproject.orderservice.model.ProductModel;
import com.epo.trainingproject.orderservice.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping("product/register")
    public ProductModel register(@RequestBody ProductModel productModel){
        return productService.save(productModel);
    }

    @PutMapping("product/add-stock/{id}/{amount}")
    public void addStock(@PathVariable int id, @PathVariable int amount){
        productService.updateStock(id, amount);
    }
}
